
【65.1 函数的参数入口。】

要往函数内部传递信息，主要有两类渠道。第一类是全局变量。第二类是函数的参数入口，而参数入口可以分为“普通局部变量”和“指针”这两类。“普通局部变量”的参数入口一次只能传一个数据，如果一个数组有几十个甚至上百个数据，此时“普通局部变量”就无能为力，这时不可能也写几十个甚至上百个入口参数吧（这会累坏程序员），针对这种需要输入批量数据的场合，“指针”的参数入口就因此而生，完美解决了此问题，仅用一个“指针”参数入口就能解决一个数组N个数据的入口问题。那么，什么是函数的参数入口？例子如下：

//函数声明

unsigned long PingJunZhi(unsigned char a,unsigned char b,unsigned char c,unsigned char d);

//变量定义

unsigned char Gu8Buffer\[4\]={2,6,8,4}; //4个变量分别是2，6,8,4。

unsigned long Gu32PingJunZhi; //求平均值的结果

//函数定义

unsigned long PingJunZhi(unsigned char a,unsigned char b,unsigned char c,unsigned char d)

{

 unsigned long u32PingJunZhi;

 u32PingJunZhi=(a+b+c+d)/4;

 return u32PingJunZhi;

}

 void main() //主函数

{

 //函数调用

 Gu32PingJunZhi=PingJunZhi(Gu8Buffer\[0\],Gu8Buffer\[1\],Gu8Buffer\[2\],Gu8Buffer\[3\]);

}

上面是一个求4个数据平均值的函数，在这个函数中，函数小括号的(unsigned char a,unsigned char b,unsigned char c,unsigned char d)就是4个变量的“普通局部变量”参数入口，刚才说到，如果一个数组有上百个变量，这种书写方式是很累的。如果改用“指针”入口参数的方式，例子如下：

//函数声明

unsigned long PingJunZhi(unsigned char \*pu8Buffer);

//变量定义

unsigned char Gu8Buffer\[4\]={2,6,8,4}; //4个变量分别是2，6,8,4。

unsigned long Gu32PingJunZhi; //求平均值的结果

//函数定义

unsigned long PingJunZhi(unsigned char \*pu8Buffer)

{

 unsigned long u32PingJunZhi;

 u32PingJunZhi=(pu8Buffer\[0\]+pu8Buffer\[1\]+pu8Buffer\[2\]+pu8Buffer\[3\])/4;

 return u32PingJunZhi;

}

 void main() //主函数

{

 //函数调用

 Gu32PingJunZhi=PingJunZhi(&Gu8Buffer\[0\]);//等效于Gu32PingJunZhi=PingJunZhi(Gu8Buffer)

}

上面例子中，仅用一个（unsigned char \*pu8Buffer）指针入口参数，就可以达到输入4个变量的目的（这4个变量要求是同在一个数组内）。

【65.2 const在指针参数“入口”中的作用。】

指针在函数的参数入口中，既可以做“入口”，也可以做“出口”，而C语言为了区分这两种情况，提供了const这个关键字来限定权限。如果指针加了const前缀，就为指针的权限加了紧箍咒，限定了此指针只能作为“入口”，而不能作为“出口”。如果没有加了const前缀，就像本节的函数例子，此时指针参数既可以作为“入口”,也可以作为“出口”。加const关键字有两个意义，一方面是方便阅读，通过const就知道此接口的“入口”和“出口”属性，另一方面，是为了代码的安全，对于只能作为“入口”的指针参数一旦加了const限定，万一我们不小心在函数内部对const限定的指针所关联的数据进行了更改（“更改”就意味着“出口”），C编译器在编译的时候就会有提醒或者报错，及时让我们发现程序的bug(程序的漏洞)。这部分的内容后续章节会讲到，大家先有个大概的了解，本节暂时不深入讲。

【65.3 例程练习和分析。】

现在编一个练习程序。

/\*---C语言学习区域的开始。-----------------------------------------------\*/

//函数声明

unsigned long PingJunZhi(unsigned char \*pu8Buffer);

//变量定义

unsigned char Gu8Buffer\[4\]={2,6,8,4}; //4个变量分别是2，6,8,4。

unsigned long Gu32PingJunZhi; //求平均值的结果

//函数定义

unsigned long PingJunZhi(unsigned char \*pu8Buffer)

{

 unsigned long u32PingJunZhi;

 u32PingJunZhi=(pu8Buffer\[0\]+pu8Buffer\[1\]+pu8Buffer\[2\]+pu8Buffer\[3\])/4;

 return u32PingJunZhi;

}

void main() //主函数

{

//函数调用

Gu32PingJunZhi=PingJunZhi(&Gu8Buffer\[0\]);//等效于Gu32PingJunZhi=PingJunZhi(Gu8Buffer)

 View(Gu32PingJunZhi); //把第1个数Gu32PingJunZhi发送到电脑端的串口助手软件上观察。

 while(1)

 {

 }

}

/\*---C语言学习区域的结束。-----------------------------------------------\*/

在电脑串口助手软件上观察到的程序执行现象如下：

开始...

第1个数

十进制:5

十六进制:5

二进制:101

分析：

 平均值变量Gu32PingJunZhi为5。

【65.4 如何在单片机上练习本章节C语言程序？】

直接复制前面章节中第十一节的模板程序，练习代码时只需要更改“C语言学习区域”的代码就可以了，其它部分的代码不要动。编译后，把程序下载进带串口的51学习板，通过电脑端的串口助手软件就可以观察到不同的变量数值，详细方法请看第十一节内容。
