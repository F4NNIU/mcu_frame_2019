
【67.1 指针作为数组在函数中的“入口”和“出口”。】

前面分别讲了指针的入口和出口，很多初学者误以为指针是一个“单向”的通道，其实，如果指针前面没有加const这个“紧箍咒”限定它的属性，指针是“双向”的，不是“单向”的，也就是说，指针是可以同时具备“入口”和“出口”这两种属性的。现在讲一个程序例子，求一个数组（内含4元素）的每个元素变量的整数倍的一半，所谓整数倍的一半，就是除以2，但是不带小数点，比如4的整数倍的一半是2，7的整数倍的一半是3（不是3.5），代码如下：

void Half(unsigned char \*pu8Buffer) //“求一半”的函数

{

 unsigned char u8Data\_0; //临时中间变量

 unsigned char u8Data\_1; //临时中间变量

 unsigned char u8Data\_2; //临时中间变量

 unsigned char u8Data\_3; //临时中间变量

 //从指针这个“入口”里获取需要“被除以2”的数据。

 u8Data\_0=pu8Buffer\[0\];

 u8Data\_1=pu8Buffer\[1\];

 u8Data\_2=pu8Buffer\[2\];

 u8Data\_3=pu8Buffer\[3\];

 //求数据的整数倍的一半的算法

 u8Data\_0=u8Data\_0/2;

 u8Data\_1=u8Data\_1/2;

 u8Data\_2=u8Data\_2/2;

 u8Data\_3=u8Data\_3/2;

 //最后，把计算所得的结果分别传输到指针这个“出口”

 pu8Buffer\[0\]=u8Data\_0;

 pu8Buffer\[1\]=u8Data\_1;

 pu8Buffer\[2\]=u8Data\_2;

 pu8Buffer\[3\]=u8Data\_3;

}

上述代码，为了突出“入口”和“出口”，我刻意多增加了u8Data\_0，u8Data\_1，u8Data\_2，u8Data\_3这4个临时中间变量，其实，这4个临时中间变量还可以省略的，此函数简化后的等效代码如下：

void Half(unsigned char \*pu8Buffer) //“求一半”的函数

{

 pu8Buffer\[0\]=pu8Buffer\[0\]/2;

 pu8Buffer\[1\]=pu8Buffer\[1\]/2;

 pu8Buffer\[2\]=pu8Buffer\[2\]/2;

 pu8Buffer\[3\]=pu8Buffer\[3\]/2;

}

【67.2 例程练习和分析。】

现在编一个练习程序。

/\*---C语言学习区域的开始。-----------------------------------------------\*/

//函数声明

void Half(unsigned char \*pu8Buffer);

//全局变量定义

unsigned char Gu8Buffer\[4\]={4,7,16,25}; //需要“被除以2”的数组

//函数定义

void Half(unsigned char \*pu8Buffer) //“求一半”的函数

{

 pu8Buffer\[0\]=pu8Buffer\[0\]/2;

 pu8Buffer\[1\]=pu8Buffer\[1\]/2;

 pu8Buffer\[2\]=pu8Buffer\[2\]/2;

 pu8Buffer\[3\]=pu8Buffer\[3\]/2;

}

void main() //主函数

{

 Half(&Gu8Buffer\[0\]); //计算数组的整数倍的一半。这里的“入口”和“出口”是“同一个通道”。

View(Gu8Buffer\[0\]); //把第1个数Gu8Buffer\[0\])发送到电脑端的串口助手软件上观察

View(Gu8Buffer\[1\]); //把第2个数Gu8Buffer\[1\])发送到电脑端的串口助手软件上观察

View(Gu8Buffer\[2\]); //把第3个数Gu8Buffer\[2\])发送到电脑端的串口助手软件上观察

View(Gu8Buffer\[3\]); //把第4个数Gu8Buffer\[3\])发送到电脑端的串口助手软件上观察

 while(1)

 {

 }

}

/\*---C语言学习区域的结束。-----------------------------------------------\*/

在电脑串口助手软件上观察到的程序执行现象如下：

开始...

第1个数

十进制:2

十六进制:2

二进制:10

第2个数

十进制:3

十六进制:3

二进制:11

第3个数

十进制:8

十六进制:8

二进制:1000

第4个数

十进制:12

十六进制:C

二进制:1100

分析：

 Gu8Buffer\[0\]为2。

 Gu8Buffer\[1\]为3。

 Gu8Buffer\[2\]为8。

 Gu8Buffer\[3\]为12。

【67.3 如何在单片机上练习本章节C语言程序？】

直接复制前面章节中第十一节的模板程序，练习代码时只需要更改“C语言学习区域”的代码就可以了，其它部分的代码不要动。编译后，把程序下载进带串口的51学习板，通过电脑端的串口助手软件就可以观察到不同的变量数值，详细方法请看第十一节内容。
